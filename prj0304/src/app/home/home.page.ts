import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  titulo="MinasMotors";

  cards = [
    {
      titulo: "X-ADV",
      subtitulo: "ON THE EDGE OF THE ROAD",
      conteudo: "A X-ADV deu o primeiro passo. Abriu caminho. E assumiu a liderança. Ao combinar as capacidades e o desempenho de uma moto de aventura, com o conforto, a comodidade e a facilidade de deslocamento de uma scooter de grande porte, este verdadeiro “SUV de duas rodas” desafia as convenções.",
      foto: "https://www.honda.com.br/motos/sites/hda/files/2022-03/19_pont.webp"
    },
    {
      titulo: "Honda CB 500F 2022",
      subtitulo: "SHOW TIME",
      conteudo: "Pilote a sua CB 500F com total confiança. O formato mais cônico do novo guidão É mais resistente e se adapta a todos os tipos de pilotagem. Você tem mais segurança para comandar toda a potência das 500 cc.",
      foto: "https://www.honda.com.br/motos/sites/hda/files/2022-03/CB-500F-laranja.webp"
    },
    {
      titulo: "Honda CB Twister 2022",
      subtitulo: "Terceira Quem vê fica impressionado. Quem pilota fica impressionante",                 
      conteudo: "Com a CB Twister, você tem conforto para ir cada vez mais longe. Contando com assento largo em dois níveis e uma excelente camada de espuma, sua pilotagem será mais do que impressionante!",
      foto: "https://www.honda.com.br/motos/sites/hda/files/2022-03/CB-Twister-vermelha.png"
    },
    {
      titulo: "CB 1000R Black Edition",
      subtitulo: "EVOLUÇÃO NA MÁXIMA POTÊNCIA",
      conteudo: "Musculosa e agressiva, a CB 1000R foi a precursora do estilo Neo Sports Café, combinando um design ainda mais minimalista com a as clássicas linhas das Café Racers. A curta rabeta e o novo farol de LED em formato de gota complementam a esportividade.",
      foto: "https://www.honda.com.br/motos/sites/hda/files/2022-02/19_1.webp"
    }
  ];

  
  
  
  constructor() {}

}
